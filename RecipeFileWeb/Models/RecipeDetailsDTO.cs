﻿using RecipeFileData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecipeFileWeb.Models
{
    public class RecipeDetailsDTO
    {
        public int RecipeId { get; set; }

        public string RecipeName { get; set; }

        public string Description { get; set; }

        public int RecipeCategoryId { get; set; }

        public string RecipeCategoryName { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public int? PhotoId { get; set; }

        public string SourceText { get; set; }

        public string SourceLink { get; set; }

        public int? SourceId { get; set; }

        public IEnumerable<RecipeItem> RecipeItems { get; set; }

        public IEnumerable<RecipeStep> RecipeSteps { get; set; }
    }
}