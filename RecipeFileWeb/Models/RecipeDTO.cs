﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecipeFileWeb.Models
{
    public class RecipeDTO
    {
        public int RecipeId { get; set; }

        public string RecipeName { get; set; }

        public string Description { get; set; }
    }
}