﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecipeFileWeb.Models
{
    public class RecipeCategoryDTO
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}