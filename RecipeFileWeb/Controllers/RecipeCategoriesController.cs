﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RecipeFileWeb.Models;
using RecipeFileData;

namespace RecipeFileWeb.Controllers
{
    public class RecipeCategoriesController : ApiController
    {
        // GET api/RecipeCategories
        [ResponseType(typeof(IEnumerable<RecipeCategoryDTO>))]
        public IEnumerable<RecipeCategoryDTO> GetAllRecipeCategories()
        {
            var db = new RecipeModel();
            var categories = from rc in db.RecipeCategories
                             select new RecipeCategoryDTO()
                             {
                                 CategoryId = rc.CategoryId,
                                 CategoryName = rc.CategoryName
                             };

            return categories;
        }

        // GET api/RecipeCategories/5
        [ResponseType(typeof(RecipeCategoryDTO))]
        public IHttpActionResult GetRecipeCategory(int id)
        {
            var db = new RecipeModel();
            var catResult = from rc in db.RecipeCategories
                            where rc.CategoryId == id
                            select new RecipeCategoryDTO()
                            {
                                CategoryId = rc.CategoryId,
                                CategoryName = rc.CategoryName
                            };
            var category = catResult.FirstOrDefault();

            if (category == null)
            {
                return NotFound();
            }
            return Ok(category);
        }

        [Route("api/recipecategories/{categoryId}/recipes")]
        [ResponseType(typeof(IEnumerable<RecipeDTO>))]
        public IEnumerable<RecipeDTO> GetRecipesByCategory(int categoryId)
        {
            var db = new RecipeModel();
            var recipes = from r in db.Recipes
                          where r.RecipeCategoryId == categoryId
                          orderby r.RecipeName
                          select new RecipeDTO()
                          {
                              RecipeId = r.RecipeId,
                              RecipeName = r.RecipeName,
                              Description = r.Description,
                          };

            return recipes;
        }

        // POST: api/CategoryRecipes
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CategoryRecipes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CategoryRecipes/5
        public void Delete(int id)
        {
        }
    }
}