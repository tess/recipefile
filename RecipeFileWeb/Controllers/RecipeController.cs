﻿using RecipeFileData;
using RecipeFileWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RecipeFileWeb.Controllers
{
    public class RecipeController : ApiController
    {
        // GET: api/Recipe
        [ResponseType(typeof(IEnumerable<RecipeDTO>))]
        public IEnumerable<RecipeDTO> Get()
        {
            var db = new RecipeModel();
            var recipes = from r in db.Recipes
                          select new RecipeDTO()
                          {
                              RecipeId = r.RecipeId,
                              RecipeName = r.RecipeName,
                              Description = r.Description
                          };

            return recipes;
        }

        // GET: api/Recipe/5
        [ResponseType(typeof(RecipeDetailsDTO))]
        public RecipeDetailsDTO Get(int id)
        {
            var db = new RecipeModel();
            var recipe = from r in db.Recipes
                         where r.RecipeId == id
                         join rc in db.RecipeCategories on r.RecipeCategoryId equals rc.CategoryId
                         join cu in db.ClientUsers on r.UserId equals cu.UserId
                         select new RecipeDetailsDTO()
                         {
                             RecipeId = r.RecipeId,
                             RecipeName = r.RecipeName,
                             Description = r.Description,
                             RecipeCategoryId = r.RecipeCategoryId,
                             RecipeCategoryName = rc.CategoryName,
                             UserId = r.UserId,
                             UserName = cu.Name,
                             PhotoId = r.PhotoId,
                             SourceText = r.SourceTextIsLink ? null : r.SourceText,
                             SourceLink = r.SourceTextIsLink ? r.SourceText : null,
                             SourceId = r.SourceId,
                         };

            return recipe.FirstOrDefault();
        }

        // POST: api/Recipe
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Recipe/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Recipe/5
        public void Delete(int id)
        {
        }
    }
}
