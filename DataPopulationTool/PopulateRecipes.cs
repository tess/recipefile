﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeFileData;

namespace DataPopulationTool
{
    public static class PopulateRecipes
    {
        public static void Populate(RecipeModel db)
        {
            var catResult = from rc in db.RecipeCategories
                            where rc.CategoryName == "Entrees"
                            select rc;
            var entrees = catResult.FirstOrDefault();

            var userResult = from cu in db.ClientUsers
                            where cu.Email == "tess@outlook.com"
                            select cu;
            var tessa = userResult.FirstOrDefault();

            var recipe = new Recipe {
                RecipeName = "Vegetarian Chili",
                Description = "A basic chili with no meat that's inexpensive to make.",
                RecipeCategory = entrees,
                ClientUser = tessa,
            };
            db.Recipes.Add(recipe);

            var ingredients = from i in db.Ingredients select i;

            var units = from u in db.Units select u;

            var recipeItem = new RecipeItem {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "pinto beans").FirstOrDefault(),
                Quantity = 1.0,
                Unit = units.Where(u => u.UnitName == "pound").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "cooking oil").FirstOrDefault(),
                Quantity = 2.0,
                Unit = units.Where(u => u.UnitName == "Tbsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "flour").FirstOrDefault(),
                Quantity = 4.0,
                Unit = units.Where(u => u.UnitName == "Tbsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "cumin").FirstOrDefault(),
                Quantity = 1.5,
                Unit = units.Where(u => u.UnitName == "Tbsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "chili powder").FirstOrDefault(),
                Quantity = 3.0,
                Unit = units.Where(u => u.UnitName == "Tbsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "garlic powder").FirstOrDefault(),
                Quantity = 1.0,
                Unit = units.Where(u => u.UnitName == "tsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "salt").FirstOrDefault(),
                Quantity = 0.5,
                Unit = units.Where(u => u.UnitName == "tsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "black pepper").FirstOrDefault(),
                Quantity = 2.0,
                Unit = units.Where(u => u.UnitName == "tsp").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem
            {
                Recipe = recipe,
                Ingredient = ingredients.Where(i => i.IngredientName == "diced tomatoes").FirstOrDefault(),
                Quantity = 1.0,
                Unit = units.Where(u => u.UnitName == "large can").FirstOrDefault(),
            };
            db.RecipeItems.Add(recipeItem);

            var recipeStep = new RecipeStep {
                Recipe = recipe,
                RecipeStepOrder = 1,
                RecipeStepText = "Hydrate beans and cook until soft all the way through."
            };
            db.RecipeSteps.Add(recipeStep);

            recipeStep = new RecipeStep
            {
                Recipe = recipe,
                RecipeStepOrder = 2,
                RecipeStepText = "In a large pot, add the cooking oil on medium heat."
            };
            db.RecipeSteps.Add(recipeStep);

            recipeStep = new RecipeStep
            {
                Recipe = recipe,
                RecipeStepOrder = 3,
                RecipeStepText = "Once the oil is heated, add the flour to make a roux. Cook until fully browned, stirring frequently."
            };
            db.RecipeSteps.Add(recipeStep);

            recipeStep = new RecipeStep
            {
                Recipe = recipe,
                RecipeStepOrder = 4,
                RecipeStepText = "Add beans with liquid from cooking and stir."
            };
            db.RecipeSteps.Add(recipeStep);

            recipeStep = new RecipeStep
            {
                Recipe = recipe,
                RecipeStepOrder = 5,
                RecipeStepText = "Add spices and mix thoroughly."
            };
            db.RecipeSteps.Add(recipeStep);

            recipeStep = new RecipeStep
            {
                Recipe = recipe,
                RecipeStepOrder = 6,
                RecipeStepText = "Mix in tomatoes and cook on high until pot boils."
            };
            db.RecipeSteps.Add(recipeStep);

            recipeStep = new RecipeStep
            {
                Recipe = recipe,
                RecipeStepOrder = 7,
                RecipeStepText = "Turn down heat and let side for 1 hour.."
            };
            db.RecipeSteps.Add(recipeStep);

            db.SaveChanges();
        }
    }
}
