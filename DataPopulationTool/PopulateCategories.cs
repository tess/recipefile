﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeFileData;

namespace DataPopulationTool
{
    public class PopulateCategories
    {
        public static void Populate(RecipeModel db)
        {
            var recipeCategory = new RecipeCategory { CategoryName = "Entrees" };
            db.RecipeCategories.Add(recipeCategory);

            recipeCategory = new RecipeCategory { CategoryName = "Appetizers" };
            db.RecipeCategories.Add(recipeCategory);

            recipeCategory = new RecipeCategory { CategoryName = "Side Dishes" };
            db.RecipeCategories.Add(recipeCategory);

            recipeCategory = new RecipeCategory { CategoryName = "Soups" };
            db.RecipeCategories.Add(recipeCategory);

            recipeCategory = new RecipeCategory { CategoryName = "Salads" };
            db.RecipeCategories.Add(recipeCategory);

            recipeCategory = new RecipeCategory { CategoryName = "Desserts" };
            db.RecipeCategories.Add(recipeCategory);

            db.SaveChanges();
        }
    }
}
