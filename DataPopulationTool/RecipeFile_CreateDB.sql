USE [Recipe]
GO

/****** Object:  Table [dbo].[RecipeStep]    Script Date: 2/13/2018 7:44:02 PM ******/
IF OBJECT_ID('dbo.RecipeStep', 'U') IS NOT NULL 
  DROP TABLE dbo.RecipeStep;
GO

/****** Object:  Table [dbo].[RecipeItem]    Script Date: 2/13/2018 7:44:02 PM ******/
IF OBJECT_ID('dbo.RecipeItem', 'U') IS NOT NULL 
  DROP TABLE dbo.RecipeItem;
GO

/****** Object:  Table [dbo].[Recipe]    Script Date: 2/13/2018 7:43:21 PM ******/
IF OBJECT_ID('dbo.Recipe', 'U') IS NOT NULL 
  DROP TABLE dbo.Recipe;
GO

/****** Object:  Table [dbo].[RecipeCategory]    Script Date: 2/13/2018 7:43:00 PM ******/
IF OBJECT_ID('dbo.RecipeCategory', 'U') IS NOT NULL 
  DROP TABLE dbo.RecipeCategory;
GO

/****** Object:  Table [dbo].[Ingredient]    Script Date: 2/13/2018 7:43:00 PM ******/
IF OBJECT_ID('dbo.Ingredient', 'U') IS NOT NULL 
  DROP TABLE dbo.Ingredient;
GO

/****** Object:  Table [dbo].[Unit]    Script Date: 2/13/2018 7:43:00 PM ******/
IF OBJECT_ID('dbo.Unit', 'U') IS NOT NULL 
  DROP TABLE dbo.Unit;
GO

/****** Object:  Table [dbo].[UnitClass]    Script Date: 2/13/2018 7:43:00 PM ******/
IF OBJECT_ID('dbo.UnitClass', 'U') IS NOT NULL 
  DROP TABLE dbo.UnitClass;
GO

/****** Object:  Table [dbo].[Photo]    Script Date: 2/13/2018 7:43:00 PM ******/
IF OBJECT_ID('dbo.Photo', 'U') IS NOT NULL 
  DROP TABLE dbo.Photo;
GO

/****** Object:  Table [dbo].[User]    Script Date: 2/13/2018 7:43:00 PM ******/
IF OBJECT_ID('dbo.ClientUser', 'U') IS NOT NULL 
  DROP TABLE dbo.ClientUser;
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[ClientUser]    Script Date: 2/13/2018 7:43:00 PM ******/
CREATE TABLE [dbo].[ClientUser](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[PasswordHash] [nvarchar](256) NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[Verified] bit NOT NULL,
	[Bio] [ntext] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ClientUser]
ADD CONSTRAINT PK_ClientUser_UserId PRIMARY KEY CLUSTERED (UserId);  
GO  

ALTER TABLE [dbo].[ClientUser]
ADD CONSTRAINT AK_ClientUser_Email UNIQUE (Email);
GO  

ALTER TABLE dbo.ClientUser
ADD CONSTRAINT DEF_ClientUser_Verified
DEFAULT 0 FOR Verified;
GO  

/****** Object:  Table [dbo].[Photo]    Script Date: 2/13/2018 7:43:00 PM ******/
CREATE TABLE Photo
(
	[PhotoId] [int] IDENTITY(1,1) NOT NULL,
	[PhotoName] varChar(50) NULL,
	[PhotoData] varBinary(MAX) NOT NULL
);

ALTER TABLE [dbo].[Photo]
ADD CONSTRAINT PK_Photo_PhotoId PRIMARY KEY CLUSTERED (PhotoId);  
GO  

/****** Object:  Table [dbo].[UnitClass]    Script Date: 2/13/2018 7:43:00 PM ******/
CREATE TABLE [dbo].[UnitClass](
	[UnitClassId] [int] IDENTITY(1,1) NOT NULL,
	[UnitClassName] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UnitClass]
ADD CONSTRAINT PK_UnitClass_UnitClassId PRIMARY KEY CLUSTERED (UnitClassId);  
GO  

ALTER TABLE [dbo].[UnitClass]
ADD CONSTRAINT AK_UnitClass_UnitClassName UNIQUE (UnitClassName);
GO  

/****** Object:  Table [dbo].[Unit]    Script Date: 2/13/2018 7:43:00 PM ******/
CREATE TABLE [dbo].[Unit](
	[UnitId] [int] IDENTITY(1,1) NOT NULL,
	[UnitName] [nvarchar](50) NOT NULL,
	[UnitNamePlural] [nvarchar](50) NULL,
	[UnitClassId] [int] NOT NULL,
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Unit]
ADD CONSTRAINT PK_Unit_UnitId PRIMARY KEY CLUSTERED (UnitId);  
GO  

ALTER TABLE [dbo].[Unit]
ADD CONSTRAINT AK_Unit_UnitName UNIQUE (UnitName);
GO  

ALTER TABLE [dbo].[Unit]
ADD CONSTRAINT FK_Unit_UnitClass FOREIGN KEY (UnitClassId)
    REFERENCES [dbo].[UnitClass] (UnitClassId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;    
GO   

/****** Object:  Table [dbo].[Ingredient]    Script Date: 2/13/2018 7:43:00 PM ******/
CREATE TABLE [dbo].[Ingredient](
	[IngredientId] [int] IDENTITY(1,1) NOT NULL,
	[IngredientName] [nvarchar](128) NOT NULL,
	[UnitClassId] [int] NOT NULL,
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Ingredient]
ADD CONSTRAINT PK_Ingredient_IngredientId PRIMARY KEY CLUSTERED (IngredientId);  
GO  

ALTER TABLE [dbo].[Ingredient]
ADD CONSTRAINT AK_Ingredient_IngredientName UNIQUE (IngredientName);
GO  

ALTER TABLE [dbo].[Ingredient]
ADD CONSTRAINT FK_Ingredient_UnitClass FOREIGN KEY (UnitClassId)
    REFERENCES [dbo].[UnitClass] (UnitClassId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;    
GO

/****** Object:  Table [dbo].[RecipeCategory]    Script Date: 2/13/2018 7:43:00 PM ******/
CREATE TABLE [dbo].[RecipeCategory](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RecipeCategory]
ADD CONSTRAINT PK_RecipeCategory_CategoryId PRIMARY KEY CLUSTERED (CategoryId);  
GO  

ALTER TABLE [dbo].[RecipeCategory]
ADD CONSTRAINT AK_RecipeCategory_CategoryName UNIQUE (CategoryName);
GO  

/****** Object:  Table [dbo].[Recipe]    Script Date: 2/13/2018 7:43:21 PM ******/
CREATE TABLE [dbo].[Recipe](
	[RecipeId] [int] IDENTITY(1,1) NOT NULL,
	[RecipeName] [nvarchar](50) NOT NULL,
	[Description] [ntext] NULL,
	[RecipeCategoryId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[PhotoId] [int] NULL,
	[SourceText] [nvarchar](512) NULL,
	[SourceTextIsLink] [bit] NOT NULL,
	[SourceId] [int] NULL,
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Recipe]
ADD CONSTRAINT PK_Recipe_RecipeID PRIMARY KEY CLUSTERED (RecipeID);
GO  

ALTER TABLE [dbo].[Recipe]
ADD CONSTRAINT AK_Recipe_RecipeName UNIQUE (RecipeName);
GO  

ALTER TABLE [dbo].[Recipe]
ADD CONSTRAINT FK_Recipe_RecipeCategory FOREIGN KEY (RecipeCategoryId)
    REFERENCES [dbo].[RecipeCategory] (CategoryId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;    
GO   

ALTER TABLE [dbo].[Recipe]
ADD CONSTRAINT FK_Recipe_User FOREIGN KEY (UserId)
    REFERENCES [dbo].[ClientUser] (UserId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;    
GO   

ALTER TABLE [dbo].[Recipe]
ADD CONSTRAINT FK_Recipe_Photo FOREIGN KEY (PhotoId)
    REFERENCES [dbo].[Photo] (PhotoId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE    
;    
GO   

ALTER TABLE dbo.[Recipe]
ADD CONSTRAINT DEF_SourceTextIsLink
DEFAULT 0 FOR SourceTextIsLink;
GO  

/****** Object:  Table [dbo].[RecipeItem]    Script Date: 2/13/2018 7:44:02 PM ******/
CREATE TABLE [dbo].[RecipeItem](
	[RecipeItemId] [int] IDENTITY(1,1) NOT NULL,
	[IngredientId] [int] NOT NULL,
	[Quantity] [float] NULL,
	[UnitId] int NOT NULL,
	[RecipeId] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RecipeItem]
ADD CONSTRAINT PK_RecipeItem_RecipeItemID PRIMARY KEY CLUSTERED (RecipeItemID);
GO  

ALTER TABLE [dbo].[RecipeItem]
ADD CONSTRAINT FK_RecipeItem_Recipe FOREIGN KEY (RecipeId)
    REFERENCES [dbo].[Recipe] (RecipeId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE
;    
GO   

ALTER TABLE [dbo].[RecipeItem]
ADD CONSTRAINT FK_RecipeItem_Ingredient FOREIGN KEY (IngredientId)
    REFERENCES [dbo].[Ingredient] (IngredientId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE
;    
GO   

ALTER TABLE [dbo].[RecipeItem]
ADD CONSTRAINT FK_RecipeItem_Unit FOREIGN KEY (UnitId)
    REFERENCES [dbo].[Unit] (UnitId)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
;    
GO   

/****** Object:  Table [dbo].[RecipeStep]    Script Date: 2/13/2018 7:44:02 PM ******/
CREATE TABLE [dbo].[RecipeStep](
	[RecipeStepId] [int] IDENTITY(1,1) NOT NULL,
	[RecipeId] [int] NOT NULL,
	[RecipeStepOrder] [int] NOT NULL,
	[RecipeStepText] [ntext] NULL,
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RecipeStep]
ADD CONSTRAINT PK_RecipeStep_RecipeStepID PRIMARY KEY CLUSTERED (RecipeStepID);
GO  

ALTER TABLE [dbo].[RecipeStep]
ADD CONSTRAINT AK_RecipeStep_RecipeId_Order UNIQUE (RecipeId, RecipeStepOrder);
GO  

ALTER TABLE [dbo].[RecipeStep]
ADD CONSTRAINT FK_RecipeStep_Recipe FOREIGN KEY (RecipeId)
    REFERENCES [dbo].[Recipe] (RecipeId)
    ON DELETE CASCADE    
    ON UPDATE CASCADE
;    
GO   








