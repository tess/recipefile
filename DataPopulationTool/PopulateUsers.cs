﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeFileData;

namespace DataPopulationTool
{
    public class PopulateUsers
    {
        public static void Populate(RecipeModel db)
        {
            var user = new ClientUser { Email = "tess@outlook.com", Name="Tessa Norris", Verified=true };
            user.SetPassword("password");
            db.ClientUsers.Add(user);

            user = new ClientUser { Email = "alice@foo.com", Name = "Alice Testperson", Verified = true };
            user.SetPassword("abc");
            db.ClientUsers.Add(user);

            user = new ClientUser { Email = "bob@foo.com", Name = "Bob Testperson", Verified = true };
            user.SetPassword("123");
            db.ClientUsers.Add(user);

            db.SaveChanges();
        }
    }
}
