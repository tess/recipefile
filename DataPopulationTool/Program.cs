﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeFileData;

namespace DataPopulationTool
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new RecipeModel())
            {
                CreateDbTables.Execute(db);
                PopulateUsers.Populate(db);
                PopulateUnits.Populate(db);
                PopulateCategories.Populate(db);
                PopulateIngredients.Populate(db);
                PopulateRecipes.Populate(db);
            }
        }
    }
}
