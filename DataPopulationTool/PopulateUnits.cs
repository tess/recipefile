﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeFileData;

namespace DataPopulationTool
{
    public static class PopulateUnits
    {
        public static void Populate(RecipeModel db)
        {
            var unitClass = new UnitClass { UnitClassName="Volume" };
            db.UnitClasses.Add(unitClass);

            var unit = new Unit { UnitName = "tsp", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "Tbsp", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "oz", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "cup", UnitNamePlural = "cups", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "pint", UnitNamePlural = "pints", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "quart", UnitNamePlural = "quarts", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "gallon", UnitNamePlural = "gallons", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "mL", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "liter", UnitNamePlural = "liters", UnitClass = unitClass };
            db.Units.Add(unit);

            unitClass = new UnitClass { UnitClassName = "Weight" };
            db.UnitClasses.Add(unitClass);

            unit = new Unit { UnitName = "pound", UnitNamePlural = "pounds", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "oz weight", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "gram", UnitNamePlural = "grams", UnitClass = unitClass };
            db.Units.Add(unit);

            unitClass = new UnitClass { UnitClassName = "Package" };
            db.UnitClasses.Add(unitClass);

            unit = new Unit { UnitName = "box", UnitNamePlural = "boxes", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "can", UnitNamePlural = "cans", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "large can", UnitNamePlural = "large cans", UnitClass = unitClass };
            db.Units.Add(unit);

            unit = new Unit { UnitName = "package", UnitNamePlural = "packages", UnitClass = unitClass };
            db.Units.Add(unit);

            db.SaveChanges();
        }
    }
}
