﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeFileData;

namespace DataPopulationTool
{
    public static class PopulateIngredients
    {
        public static void Populate(RecipeModel db)
        {
            var result = from uc in db.UnitClasses
                         where uc.UnitClassName == "Weight"
                         select uc;
            var weight = result.FirstOrDefault();

            result = from uc in db.UnitClasses
                     where uc.UnitClassName == "Package"
                     select uc;
            var package = result.FirstOrDefault();

            result = from uc in db.UnitClasses
                     where uc.UnitClassName == "Volume"
                     select uc;
            var volume = result.FirstOrDefault();

            var ingredient = new Ingredient { IngredientName = "pinto beans", UnitClass = weight };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "cooking oil", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "flour", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "diced tomatoes", UnitClass = package };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "cumin", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "chili powder", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "garlic powder", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "salt", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            ingredient = new Ingredient { IngredientName = "black pepper", UnitClass = volume };
            db.Ingredients.Add(ingredient);

            db.SaveChanges();
        }
    }
}
