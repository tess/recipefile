﻿using RecipeFileData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPopulationTool
{
    public static class CreateDbTables
    {
        public static void Execute(RecipeModel db)
        {
            string script = File.ReadAllText(@"RecipeFile_CreateDB.sql");
            var separators = new string[] { "GO" };
            string[] commands = script.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            foreach (var command in commands) {
                if(String.IsNullOrWhiteSpace(command))
                {
                    continue;
                }

                db.Database.ExecuteSqlCommand(command);
            }

            db.SaveChanges();
        }
    }
}