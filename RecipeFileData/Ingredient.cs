namespace RecipeFileData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ingredient")]
    public partial class Ingredient
    {
        public int IngredientId { get; set; }

        [Required]
        [StringLength(128)]
        public string IngredientName { get; set; }

        public int UnitClassId { get; set; }

        public virtual UnitClass UnitClass { get; set; }
    }
}
