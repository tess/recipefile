namespace RecipeFileData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Recipe")]
    public partial class Recipe
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Recipe()
        {
            RecipeItems = new HashSet<RecipeItem>();
            RecipeSteps = new HashSet<RecipeStep>();
        }

        public int RecipeId { get; set; }

        [Required]
        [StringLength(50)]
        public string RecipeName { get; set; }

        public string Description { get; set; }

        public int RecipeCategoryId { get; set; }

        public int UserId { get; set; }

        public int? PhotoId { get; set; }

        [StringLength(512)]
        public string SourceText { get; set; }

        public bool SourceTextIsLink { get; set; }

        public int? SourceId { get; set; }

        public virtual ClientUser ClientUser { get; set; }

        public virtual Photo Photo { get; set; }

        public virtual RecipeCategory RecipeCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecipeItem> RecipeItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecipeStep> RecipeSteps { get; set; }
    }
}
