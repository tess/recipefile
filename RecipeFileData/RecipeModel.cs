namespace RecipeFileData
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RecipeModel : DbContext
    {
        public RecipeModel()
            : base("name=RecipeConnection")
        {
        }

        public virtual DbSet<ClientUser> ClientUsers { get; set; }
        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<Recipe> Recipes { get; set; }
        public virtual DbSet<RecipeCategory> RecipeCategories { get; set; }
        public virtual DbSet<RecipeItem> RecipeItems { get; set; }
        public virtual DbSet<RecipeStep> RecipeSteps { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<UnitClass> UnitClasses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Photo>()
                .Property(e => e.PhotoName)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .HasMany(e => e.Recipes)
                .WithOptional(e => e.Photo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<RecipeCategory>()
                .HasMany(e => e.Recipes)
                .WithRequired(e => e.RecipeCategory)
                .HasForeignKey(e => e.RecipeCategoryId)
                .WillCascadeOnDelete();
        }
    }
}
