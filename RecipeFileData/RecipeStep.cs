namespace RecipeFileData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RecipeStep")]
    public partial class RecipeStep
    {
        public int RecipeStepId { get; set; }

        public int RecipeId { get; set; }

        public int RecipeStepOrder { get; set; }

        [Column(TypeName = "ntext")]
        public string RecipeStepText { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
