namespace RecipeFileData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RecipeItem")]
    public partial class RecipeItem
    {
        public int RecipeItemId { get; set; }

        public double? Quantity { get; set; }

        public int RecipeId { get; set; }

        public virtual Recipe Recipe { get; set; }

        public int IngredientId { get; set; }

        public virtual Ingredient Ingredient { get; set; }

        public int UnitId { get; set; }

        public virtual Unit Unit { get; set; }
    }
}
