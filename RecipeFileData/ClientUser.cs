namespace RecipeFileData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Security.Cryptography;
    using System.Text;

    [Table("ClientUser")]
    public partial class ClientUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClientUser()
        {
            Recipes = new HashSet<Recipe>();
        }

        [Key]
        public int UserId { get; set; }

        [Required]
        [StringLength(128)]
        public string Email { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string PasswordHash { get; set; }

        [Required]
        [StringLength(128)]
        public string PasswordSalt { get; set; }

        public bool Verified { get; set; }

        [Column(TypeName = "ntext")]
        public string Bio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recipe> Recipes { get; set; }

        public void SetPassword(string password)
        {
            var passArray = Encoding.Unicode.GetBytes(password);

            var salt = Encoding.Unicode.GetBytes(GenerateSalt());
            this.PasswordSalt = Convert.ToBase64String(salt);

            var hash = GenerateSaltedHash(passArray, salt);
            this.PasswordHash = Convert.ToBase64String(hash);
        }

        public bool CheckPassword(string password)
        {
            var passArray = Encoding.Unicode.GetBytes(password);
            var salt = Encoding.Unicode.GetBytes(this.PasswordSalt);
            var testHash = GenerateSaltedHash(passArray, salt);

            return testHash == Encoding.Unicode.GetBytes(this.PasswordHash);
        }

        // from: https://codereview.stackexchange.com/questions/93614/salt-generation-in-c
        private static string GenerateSalt()
        {
            var random = new RNGCryptoServiceProvider();

            // Maximum length of salt
            int max_length = 32;

            // Empty salt array
            byte[] salt = new byte[max_length];

            // Build the random bytes
            random.GetNonZeroBytes(salt);

            // Return the string encoded salt
            return Convert.ToBase64String(salt);
        }

        // from: https://stackoverflow.com/questions/2138429/hash-and-salt-passwords-in-c-sharp
        static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
              new byte[plainText.Length + salt.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainText[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[plainText.Length + i] = salt[i];
            }

            return algorithm.ComputeHash(plainTextWithSaltBytes);
        }
    }
}
