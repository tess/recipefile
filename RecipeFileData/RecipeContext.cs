﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeFileData
{
    public partial class RecipeContext : DbContext
    {
        public RecipeContext()
            : base("name=RecipeContext")
        {
        }

        public virtual DbSet<ClientUser> ClientUsers { get; set; }
        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<Recipe> Recipes { get; set; }
        public virtual DbSet<RecipeCategory> RecipeCategories { get; set; }
        public virtual DbSet<RecipeItem> RecipeItems { get; set; }
        public virtual DbSet<RecipeModel> RecipeModels { get; set; }
        public virtual DbSet<RecipeStep> RecipeSteps { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<UnitClass> UnitClasses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public static RecipeContext GetContext()
        {
            return new RecipeContext();
        }
    }
}
