namespace RecipeFileData
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Unit")]
    public partial class Unit
    {
        public int UnitId { get; set; }

        [Required]
        [StringLength(50)]
        public string UnitName { get; set; }

        [StringLength(50)]
        public string UnitNamePlural { get; set; }

        public int UnitClassId { get; set; }

        public virtual UnitClass UnitClass { get; set; }
    }
}
